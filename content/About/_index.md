+++
title = "About"
description = ""
weight = 1
+++

{{< lead >}}
This is a collaboration of like-minded organizations and individuals who pool their resources to promote digital platform strategy, tooling, and patterns to promote Day 2 operational mindsets.
{{< /lead >}} 

{{< childpages >}}
