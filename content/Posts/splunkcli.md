+++
title = "Splunk CLI"
description = ""
weight = 1
+++
by: [Allen Plummer](allen@73prime.io)

I wrote a CLI app for Splunk Enterprise's REST API. This likely works also for the SplunkCloud implementation.

The purpose of this CLI is to help facilitate CI/CD processes. In other words, you put this CLI into a Runner/Agent to scan a Git repo with the Splunk XML/YAML files that represent dashboards/alerts, and it will provision your Splunk instance.

Note: The Alerting isn't complete yet.

[Splunk CLI](https://gitlab.com/73prime/splunkcli)