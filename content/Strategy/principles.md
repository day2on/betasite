+++
title = "Principles"
description = ""
weight = 1
+++

{{< lead >}}
Having a principled approach to making decisions is a key starting place to keeping "Day 2" in mind.
{{< /lead >}}