+++
title = "Strategy"
description = ""
weight = 2
+++

{{< lead >}}
Strategy without Tactics is the slowest path to victory; tactics without strategy is the noise before defeat.
{{< /lead >}}

It takes a balanced approach, balancing business needs with technology strategy. 

{{< childpages >}}
