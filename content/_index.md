---
title: ""
date: 2021-11-25T11:38:33-06:00
draft: false
---

# Oh, for the love of Ops! 

"Day 2" represents your environment after that gnarly development project is complete. Are you planning for Day 2 On while in the midst of the Day 1 tyranny of the urgent?

* The developers are gone.
* Money has run out.
* The business has lost interest.

## Yet, somebody has to:

* Tend to production outages, upgrades, and maintenance.
* Manage the code delivery systems that are likely manual in nature.
* Hold on to the tribal knowledge.
* Be the hero.

## What if:

* All development was started with "Day 2" in mind?
* "DevOps" wasn't a thing to hire out for...that the platform had an "on rails" method by which developers could deploy their code?
* Observability was baked in?


## Key Outcomes
<div class="row py-3 mb-5">
    <div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="fas fa-tachometer-alt fa-2x text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					Platform Experimentation.
				</h5>
				<p class="card-text text-muted">
					Focus your experimentation not on runtime technology, but on business logic. Don't use technology without connecting it to business value.
				</p>
			</div>
		</div>
	</div>
    <div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="fas fa-cogs fa-2x text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					Platform Enablement.
				</h5>
				<p class="card-text text-muted">
					Prescriptive platforms free operations teams up to work on Chaos Engineering and Synthetic Testing. Simplification is key.
				</p>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="fas fa-project-diagram fa-2x text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					Platform Services.
				</h5>
				<p class="card-text text-muted">
					Common tooling in your platform enables all projects deployed to inherit sweet optics, and other common services
				</p>
			</div>
		</div>
	</div>
</div>


<p style="text-align: center;">Platform Solutions for the Enterprise</p>
